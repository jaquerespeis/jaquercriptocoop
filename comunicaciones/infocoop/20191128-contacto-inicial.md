Hola,

Mi nombre es [nombre]. Con un+s amig+s tenemos un colectivo, y nos gustaría
formar una cooperativa de ahorro.

¿Qué debo hacer para que me asesoren sobre cómo empezar, cuáles son los riesgos, y problemas que probablemente nos vamos a encontrar?

Gracias,
pura vida

---

Buenas tardes [nombre],

Para iniciar una cooperativa se necesita una idea productiva o un proyecto concreto, interés de organizarse conjuntamente con otras personas (como mínimo deben ser 12 o 20 personas según el tipo de cooperativa), disposición al diálogo y a tomar decisiones colectivas. Los pasos para crear una cooperativa son: crear el grupo, presentar el proyecto empresarial, elaborar los estatutos, realizar la asamblea constitutiva y tramitar la inscripción.

Se necesita contar con un mínimo de 20 personas si se trata de una cooperativa tradicional o un mínimo de 12 si es una cooperativa autogestionaria (en donde los asociados son trabajadores de la cooperativa).

Este grupo precooperativo debe formar un Comité Central de Organización para dar sus primeros pasos para crear e inscribir la cooperativa.

El comité tendrá también la función principal de organizar al grupo, así como preparar la Asamblea Constitutiva, donde se elige al presidente, secretario, tesorero y dos vocales.

Visite nuestra página web https://www.infocoop.go.cr/servicios/servicios-promocion para encontrar material que le sirva de apoyo e infórmese sobre los demás pasos a seguir para la formación de su cooperativa.

Cordialmente,

---

Muchas gracias por la información.
Ya tenemos unas 10 personas intersadas, y encontrar las demás creo que no será
mucho problema.

Tengo muchas preguntas sobre factibilidad y legalidad. No se qué es posible
hacer en una cooperativa de ahorro y qué no. Supongo que deben haber muchos
requisitos para demostrar que no es una organización de lavado de dinero o
evasión fiscal, pero no se cómo cumplir esos requisitos. ¿Será posible
reunirme con alguien que me ayude a resolver estas preguntas?

Por la sostenibilidad del proyecto no me preocupa, porque el tema de ahorro es
sencillo.

pura vida.

---

Buenos días,

Puede comunicarse directamente con el área de Promoción al 2256-2944 extensión 2301 o al correo promocion@infocoop.go.cr.

Cordialmente,

---